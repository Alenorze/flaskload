import os
import string
import random
from flask import Flask, flash, render_template, request, send_file
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from io import BytesIO

from forms import DownloadForm


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'dev.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "secret"
app.config['DEBUG'] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)



class FileContents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), unique=False)
    description = db.Column(db.String(300))
    data = db.Column(db.LargeBinary)
    code = db.Column(db.String(300))


def random_string(length):
    return ''.join(random.choice(string.ascii_letters) for m in range(length))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    file = request.files['inputFile']
    description = request.form['description']
    code = random_string(10)
    newFile = FileContents(name=file.filename, data=file.read(), code=code, description=description)
    db.session.add(newFile)
    db.session.commit()
    return 'Saved ' + file.filename + ' to the database, download code: ' + code


@app.route('/download', methods=['POST', 'GET'])
def download():
    form = DownloadForm()
    code = form.code.data
    if form.is_submitted():
        file_data = FileContents.query.filter_by(code=code).first()
        return send_file(BytesIO(file_data.data), attachment_filename='name.ico', as_attachment=True)
    return render_template('download.html', form=form)


if __name__ == '__main__':
    app.run()
